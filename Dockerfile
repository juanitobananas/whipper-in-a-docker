FROM debian:jessie-slim

ENV WHIPPER_LATEST "v0.4.0"

LABEL version="v0.4.0"
LABEL description="This is just a simple docker file to \
	run whipper (morituri ripper successor): \
	https://github.com/JoeLametta/whipper"

# Install dependencies available in debian repo
RUN apt-get update && apt-get install -y libsndfile1-dev cdparanoia \
	cdrdao gstreamer0.10-plugins-base python-musicbrainzngs \
	python-setuptools python-cddb flac sox python-gobject-2 \
	python-gst0.10 gstreamer0.10-plugins-good

# Install pycdio (not available in debian repos) with pip
RUN apt-get update && apt-get install -y python-dev libcdio-dev libiso9660-dev swig\
	pkg-config python-pip
RUN pip install pycdio==0.17

# Install whipper
RUN apt-get update && apt-get install -y git
RUN git clone -b master --single-branch https://github.com/JoeLametta/whipper.git
RUN cd whipper; git checkout -b latest ${WHIPPER_LATEST}; git submodule init; git submodule update;

# Build and installed bundled dependencies
RUN cd whipper; cd src; make; make install;

# Actually install whipper
RUN cd whipper; python2 setup.py install

# Remove temporary installation files
RUN rm -rf /whipper

RUN mkdir /whipper-rips

# Add the most common commands to bash_history
RUN echo "whipper cd rip" >> /root/.bash_history; \
	echo "whipper offset find" >> /root/.bash_history; \
	echo "whipper drive analyze" >> /root/.bash_history

#!/bin/bash

CONFIGDIR="${HOME}/.whipper-in-a-docker"
RIPSDIR="${HOME}/whipper-rips"

if [[ ! -d "${CONFIGDIR}" ]]; then
	mkdir -p ${CONFIGDIR}
	cp whipper.conf.default ${CONFIGDIR}/whipper.conf
fi

docker run -it \
	--device=/dev/sr0:/dev/sr0 \
	--device=/dev/cdrom:/dev/cdrom \
	-v ${CONFIGDIR}:/root/.config/whipper/ \
	-v ${RIPSDIR}:/whipper-rips \
	whipper-in-a-docker /bin/bash

